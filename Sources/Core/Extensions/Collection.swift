import Foundation

// MARK: - Collection

public extension Collection {
    /// Returns the element at the index if whithin the bounds, otherwise `.none`.
    /// - Parameter index: index
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : .none
    }

    /// Returns the elements at the range of indices if whithin the bounds, otherwise `.none`.
    /// - Parameter bounds: range of indices
    subscript(safe bounds: Range<Index>) -> [Element]? {
        guard bounds.lowerBound >= startIndex && bounds.upperBound <= endIndex else {
            return .none
        }
        return Array(self[bounds])
    }

    /// Returns the elements at the closed range of indices if whithin the bounds, otherwise `.none`.
    /// - Parameter bounds: closed range of indices
    subscript(safe bounds: ClosedRange<Index>) -> [Element]? {
        guard bounds.lowerBound >= startIndex && bounds.upperBound < endIndex else {
            return .none
        }
        return Array(self[bounds])
    }
}

// MARK: - SafeCollection

public final class SafeCollection<T: Collection> {
    private let collection: T

    init(collection: T) {
        self.collection = collection
    }
}

public extension SafeCollection {
    /// Returns the element at the index if whithin the bounds, otherwise `.none`.
    /// - Parameter index: index
    subscript(index: T.Index) -> T.Element? {
        return collection[safe: index]
    }

    /// Returns the elements at the range of indices if whithin the bounds, otherwise `.none`.
    /// - Parameter bounds: range of indices
    subscript(bounds: Range<T.Index>) -> [T.Element]? {
        return collection[safe: bounds]
    }

    /// Returns the elements at the closed range of indices if whithin the bounds, otherwise `.none`.
    /// - Parameter bounds: closed range of indices
    subscript(bounds: ClosedRange<T.Index>) -> [T.Element]? {
        return collection[safe: bounds]
    }
}

// MARK: - Collection -> SafeCollection

public extension Collection {
    var safe: SafeCollection<Self> {
        return SafeCollection(collection: self)
    }
}
