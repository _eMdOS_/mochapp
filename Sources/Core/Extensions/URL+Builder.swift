import Foundation

// MARK: - URL.Scheme

extension URL {
    public enum Scheme: String {
        case http
        case https
    }
}

// MARK: - URL.Builder

extension URL {
    public class Builder {
        private var components: URLComponents = .init()
        
        /// Initializes the builder. By default, `https` is set as scheme.
        public init(scheme: Scheme = .https) {
            setScheme(scheme)
        }
    }
}

public extension URL.Builder {
    /// Sets the scheme subcomponent of the URL.
    /// - Parameter scheme: scheme
    @discardableResult
    func setScheme(_  scheme: URL.Scheme?) -> URL.Builder {
        components.scheme = scheme?.rawValue
        return self
    }
    
    /// Sets the host subcomponent.
    /// - Parameter host: host
    @discardableResult
    func setHost(_  host: String?) -> URL.Builder {
        components.host = host
        return self
    }
    
    /// Sets the port subcomponent.
    /// - Parameter port: port
    @discardableResult
    func setPort(_  port: Int?) -> URL.Builder {
        components.port = port
        return self
    }
    
    /// Sets the path subcomponent.
    /// - Parameter path: path
    @discardableResult
    func setPath(_ path: String) -> URL.Builder {
        components.path = path
        return self
    }
    
    /// Sets an array of  key-value pairs to be sent as query parameters.
    /// - Parameter query: query items
    @discardableResult
    func setQuery(_ query: [URLQueryItem]?) -> URL.Builder {
        components.queryItems =  query
        return self
    }
    
    /// Returns the built URL from the components set using the builder's functions.
    /// When the URL cannot be built, the functions throws a `URL.Builder.Error.invalidURL`
    @discardableResult
    func build() throws -> URL {
        guard let url = components.url else {
            throw URL.Builder.Error.invalidURL
        }
        return url
    }
}

// MARK: - URL.Builder.Error

extension URL.Builder {
    public enum Error: Swift.Error, Equatable {
        case invalidURL
    }
}

