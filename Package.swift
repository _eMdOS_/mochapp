// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "MochApp",
    platforms: [
        .iOS(.v12),
        .tvOS(.v12)
    ],
    products: [
        .library(
            name: "MochApp",
            targets: ["MochApp"]
        )
    ],
    dependencies: [],
    targets: [
        .target(
            name: "MochApp",
            dependencies: [],
            path: "./Sources"
        ),
        .testTarget(
            name: "MochAppTests",
            dependencies: ["MochApp"],
            path: "./Tests"
        )
    ],
    swiftLanguageVersions: [.v5]
)
