import XCTest
import MochApp

final class URL_Tests: XCTestCase {
    func test_init_from_static_string() {
        let url: URL = "https://api.test.com"
        let secondURL = URL(string: "https://api.test.com")
        XCTAssertEqual(url, secondURL)
    }
}
