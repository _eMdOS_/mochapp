import XCTest
import MochApp

final class Collection_Tests: XCTestCase {
    let intArray: [Int] = [0, 1, 2]
}

// MARK: - Test Cases

extension Collection_Tests {
    func test_safeAccessToAllIndices() {
        intArray
            .enumerated()
            .forEach { (index, value) in
                XCTAssertEqual(index, intArray.safe[value])
            }
    }

    func test_nilWhenIndexOutOfBounds() {
        XCTAssertNil(intArray.safe[intArray.endIndex])
    }

    func test_safeAccessByClosedRange() {
        let closedRanges: [ClosedRange<Int>] = [0...1, 0...2]
        closedRanges
            .forEach { closedRange in
                XCTAssertNotNil(intArray.safe[closedRange])
            }
    }

    func test_nilWhenClosedRangeOutOfBounds() {
        XCTAssertNil(intArray.safe[0...3])
    }

    func test_safeAccessByRange() {
        let ranges: [Range<Int>] = [0..<1, 0..<2, 0..<3]
        ranges
            .forEach { range in
                XCTAssertNotNil(intArray.safe[range])
            }
    }

    func test_nilWhenRangeOutOfBounds() {
        XCTAssertNil(intArray.safe[0..<4])
    }
}
